// popup.js
// Initializing the canvas
var canvas = document.querySelector('canvas'),
    ctx = canvas.getContext('2d');

// Setting the width and height of the canvas
canvas.width = 560;
canvas.height = 600;
//canvas.width = window.innerWidth;
//canvas.height = window.innerHeight;


// Setting up the numbers
var numbers = '01010101';
numbers = numbers.split('');

// Setting up the columns
var fontSize = 15,
    columns = canvas.width / fontSize;

// Setting up the drops
var drops = [];
for (var i = 0; i < columns; i++) {
  drops[i] = 1;
}

// Setting up the draw function
function draw() {
  ctx.fillStyle = 'rgba(0, 0, 0, .1)';
  ctx.fillRect(0, 0, canvas.width, canvas.height);
  for (var i = 0; i < drops.length; i++) {
    var text = numbers[Math.floor(Math.random() * numbers.length)];
    ctx.fillStyle = '#0f0';
    ctx.fillText(text, i * fontSize, drops[i] * fontSize);
    drops[i]++;
    if (drops[i] * fontSize > canvas.height && Math.random() > .95) {
      drops[i] = 0;
    }
  }
}

// Loop the animation
setInterval(draw, 33);

/*
function myFunction() {
    alert("test")
    var input = document.getElementById("textField").value
    const value = eval(input)
    document.getElementById("textField").value=value
}

var username = "Test1";
var password = "Test2";
*/
